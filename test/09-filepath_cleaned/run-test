#! /bin/sh

[ "${TRACE}" = 'YES' ] && set -x && : "$0" "$@"

###   ###   ###   ###   ###   ###   ###   ###   ###   ###   ###   ###   ###
MULLE_BASHFUNCTIONS_LIBEXEC_DIR="`mulle-bashfunctions libexec-dir`" || exit 1
export MULLE_BASHFUNCTIONS_LIBEXEC_DIR
. "${MULLE_BASHFUNCTIONS_LIBEXEC_DIR}/mulle-boot.sh" || exit 1
. "${MULLE_BASHFUNCTIONS_LIBEXEC_DIR}/mulle-bashfunctions.sh" || exit 1
###   ###   ###   ###   ###   ###   ###   ###   ###   ###   ###   ###   ###

if [ ! -z "${ZSH_VERSION}" ]
then
   setopt sh_word_split
fi


expect_output()
{
   local output="$1"
   local expected="$2"

   if [ "${output}" != "${expected}" ]
   then
      echo "Got \"${output}\". Expected: \"${expected}\"" >&2
      stacktrace >&2
      exit 1
   fi
}


fail()
{
   echo "Unexpected failure" >&2
   stacktrace >&2
   exit 1
}


test_filepath_cleaned()
{
   local output

# EMPTY STRING
   r_filepath_cleaned ""
   expect_output "${RVAL}" ""

# /
   r_filepath_cleaned "/"
   expect_output "${RVAL}" "/"

# //
   r_filepath_cleaned "//"
   expect_output "${RVAL}" "/"

   r_filepath_cleaned "///"
   expect_output "${RVAL}" "/"

# / a /
   r_filepath_cleaned "a/"
   expect_output "${RVAL}" "a/"

   r_filepath_cleaned "/a"
   expect_output "${RVAL}" "/a"

   r_filepath_cleaned "/a/"
   expect_output "${RVAL}" "/a/"

   r_filepath_cleaned "/a//"
   expect_output "${RVAL}" "/a/"

   r_filepath_cleaned "//a//"
   expect_output "${RVAL}" "/a/"

# / a /
   r_filepath_cleaned "a/."
   expect_output "${RVAL}" "a/."

   r_filepath_cleaned "/.a"
   expect_output "${RVAL}" "/.a"

   r_filepath_cleaned "/./a/"
   expect_output "${RVAL}" "/a/"

   r_filepath_cleaned "/a/./"
   expect_output "${RVAL}" "/a/"

   r_filepath_cleaned "/./a/./"
   expect_output "${RVAL}" "/a/"
}


main()
{
   test_filepath_cleaned

   echo "All tests passed" >&2
}


init()
{
   MULLE_BASHFUNCTIONS_LIBEXEC_DIR="${MULLE_BASHFUNCTIONS_LIBEXEC_DIR:-../../src}"

   . "${MULLE_BASHFUNCTIONS_LIBEXEC_DIR}/mulle-bashglobal.sh" || exit 1
   . "${MULLE_BASHFUNCTIONS_LIBEXEC_DIR}/mulle-compatibility.sh" || exit 1
   . "${MULLE_BASHFUNCTIONS_LIBEXEC_DIR}/mulle-string.sh" || exit 1
   . "${MULLE_BASHFUNCTIONS_LIBEXEC_DIR}/mulle-logging.sh" || exit 1
}


init "$@"
main "$@"

